# Terminal Paint - We.Publish Coding Test

This is an entry test for the company We.Publish.
This is a small paint CLI application.

It is missing unit tests for the commands on purpose.
There are integration tests based on the sample I/O defined in the document.

## Installation

This application requires NodeJs. I have tested it with the version `v14.17.4`.

```
git clone https://gitlab.com/itrulia/we.publish-test.git
cd we.publish-test
npm install
```

## Run

```
npm start
```

This will run the application. there's a few commands, see `Commands`.


## Test

```
npm test
```

This will run `jest`.


## Commands

### New Canvas

The new canvas command can be invoked by entering `C [w] [h]`.
This erases the existing canvas and the lines drawn on it.

Example: `C 6 6`

#### Argument w
Type: `Number`
Default: `undefined`

This is the width of the canvas

#### Argument h
Type: `Number`
Default: `undefined`

This is the height of the canvas


### Draw Line

The draw line command can be invoked by entering `L [x1] [y1] [x2] [y2]`.
This draws a line on the canvas using the 'x' as character.

Example: `L 1 1 6 2`

#### Argument x1
Type: `Number`
Default: `undefined`

This is the start of the line on the x-axis of the canvas.

#### Argument y1
Type: `Number`
Default: `undefined`

This is the start of the line on the y-axis of the canvas.

#### Argument x2
Type: `Number`
Default: `undefined`

This is the end of the line on the x-axis of the canvas.

#### Argument y2
Type: `Number`
Default: `undefined`

This is the end of the line on the y-axis of the canvas.



### Draw Rectangle

The draw rectangle command can be invoked by entering `R [x1] [y1] [x2] [y2]`.
This draws a rectangle on the canvas using the 'x' as character.

Example: `L 1 1 6 2`

#### Argument x1
Type: `Number`
Default: `undefined`

This is the start of the rectangle on the x-axis of the canvas.

#### Argument y1
Type: `Number`
Default: `undefined`

This is the start of the rectangle on the y-axis of the canvas.

#### Argument x2
Type: `Number`
Default: `undefined`

This is the end of the rectangle on the x-axis of the canvas.

#### Argument y2
Type: `Number`
Default: `undefined`

This is the end of the rectangle on the y-axis of the canvas.



### Bucket Fill

The bucket fill command can be invoked by entering `B [x] [y] [c]`.
This fills the canvas similar to the bucket in MSPaint.

Example: `B 2 2 a`

#### Argument x
Type: `Number`
Default: `undefined`

This is the where the bucket fill should start on the x-axis of the canvas.

#### Argument y
Type: `Number`
Default: `undefined`

This is the where the bucket fill should start on the y-axis of the canvas.

#### Argument c
Type: `Char`
Default: `undefined`

This is the character/color the bucket fill should use to fill the canvas.