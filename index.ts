import * as rl from "readline";
import { stdin as input, stdout as output } from "process";
import { executeCommand } from "./src/commands/execute";
import { Command } from "./src/commands";
import { Canvas } from "./src/canvas";
import { renderCanvas } from "./src/render";

const readline = rl.createInterface({ input, output });

const question = (questionString: string): Promise<string> =>
    new Promise((resolve) => readline.question(questionString, resolve));

const init = async () => {
    let canvas: Canvas = [];

    while (true) {
        // will be terminated by entering "Q"
        const read = await question("enter command: ");
        const commands = read.split(" ");

        canvas = executeCommand(canvas, commands as Command);
        console.log(renderCanvas(canvas));
    }
};

init();
