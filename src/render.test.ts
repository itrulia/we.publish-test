import { renderCanvas } from "./render";

test("it should render a canvas", () => {
    const canvas = [
        ["A", "x"],
        ["x", " "],
        [" ", " "],
    ];

    const result = renderCanvas(canvas);
    expect(result).toEqual(
        `
----
|Ax|
|x |
|  |
----
`
    );
});

test("it should not render a canvas without one", () => {
    const result1 = renderCanvas(null);
    expect(result1).toEqual("");

    const result2 = renderCanvas([]);
    expect(result2).toEqual("");

    const result3 = renderCanvas([[]]);
    expect(result3).toEqual("");
});
