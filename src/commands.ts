export type CanvasCommand = ["C" | "c", string, string];
export type LineCommand = ["L" | "l", string, string, string, string];
export type RectangleCommand = ["R" | "r", string, string, string, string];
export type BucketFillCommand = ["B" | "b", string, string, string];
export type QuitCommand = ["Q" | "q"];

export type Command =
    | CanvasCommand
    | LineCommand
    | RectangleCommand
    | BucketFillCommand
    | QuitCommand;

const createCommandValidator =
    <CommandType extends Command>(
        commandDeclarator: Command[0],
        argumentLengths: number
    ) =>
    (command: Command): command is CommandType => {
        const [cmd, ...args] = command;

        return (
            commandDeclarator.toLowerCase() === cmd?.[0].toLocaleLowerCase() &&
            args.length === argumentLengths
        );
    };

export const isCanvasCommand = createCommandValidator<CanvasCommand>("C", 2);
export const isLineCommand = createCommandValidator<LineCommand>("L", 4);
export const isRectangleCommand = createCommandValidator<RectangleCommand>(
    "R",
    4
);
export const isBucketFillCommand = createCommandValidator<BucketFillCommand>(
    "B",
    3
);
export const isQuitCommand = createCommandValidator<QuitCommand>("Q", 0);
