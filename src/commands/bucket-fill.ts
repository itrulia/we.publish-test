import { Canvas } from "../canvas";
import { BucketFillCommand } from "../commands";

/**
 * The logic for the flood fill algorithm is that it spreads
 * like a virus on all bordering fields (top, right, bottom and left).
 *
 * For example we have a grid off:
 *
 * xxxx
 * xxxx  <--- we are going to flood fill the 2nd to the right
 * xxxx
 *
 * We are turning this in to:
 * xxox
 * xooo
 * xxox
 *
 * Which will on the next step turn in to:
 * xooo
 * oooo
 * xooo
 *
 * And then in to:
 * oooo
 * oooo
 * oooo
 */
const floodFill = (
    canvas: Canvas,
    x: number,
    y: number,
    currentCharacter: string,
    newCharacter: string
): Canvas => {
    // x or y is out of bounds
    if (x < 0 || y < 0 || x >= canvas[0].length || y >= canvas.length) {
        return canvas;
    }

    // Since we want it to behave like MSPaint bucket fill, we want to stop once we hit a new color/character.
    if (canvas[y][x] !== currentCharacter) {
        return canvas;
    }

    canvas[y][x] = newCharacter;

    // field to the top
    floodFill(canvas, x, y - 1, currentCharacter, newCharacter);
    // field to the right
    floodFill(canvas, x + 1, y, currentCharacter, newCharacter);
    // field to the bottom
    floodFill(canvas, x, y + 1, currentCharacter, newCharacter);
    // field to the left
    floodFill(canvas, x - 1, y, currentCharacter, newCharacter);

    return canvas;
};

export const bucketFill = (
    canvas: Canvas,
    [, x, y, c]: BucketFillCommand
): Canvas => {
    if (!canvas?.[+y - 1]?.[+x - 1] || !c) {
        return canvas;
    }

    const currentCharacter = canvas[+y - 1][+x - 1];

    return floodFill(canvas, +x - 1, +y - 1, currentCharacter, c);
};
