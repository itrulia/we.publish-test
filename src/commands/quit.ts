import { exit } from "process";

export const quit = (): void => {
    exit(0);
};
