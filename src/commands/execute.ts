import { Canvas } from "../canvas";
import {
    Command,
    isBucketFillCommand,
    isCanvasCommand,
    isLineCommand,
    isQuitCommand,
    isRectangleCommand,
} from "../commands";
import { bucketFill } from "./bucket-fill";
import { drawLine } from "./draw-line";
import { drawRectangle } from "./draw-rectangle";
import { createNewCanvas } from "./new-canvas";
import { quit } from "./quit";

export const executeCommand = (canvas: Canvas, command: Command): Canvas => {
    if (isCanvasCommand(command)) {
        return createNewCanvas(command);
    }

    if (isLineCommand(command)) {
        return drawLine(canvas, command);
    }

    if (isRectangleCommand(command)) {
        return drawRectangle(canvas, command);
    }

    if (isBucketFillCommand(command)) {
        return bucketFill(canvas, command);
    }

    if (isQuitCommand(command)) {
        quit();
        return canvas;
    }

    return canvas;
};
