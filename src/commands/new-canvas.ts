import { Canvas } from "../canvas";
import { CanvasCommand } from "../commands";

export const createNewCanvas = ([, w, h]: CanvasCommand): Canvas => {
    if (+w <= 0 || +h <= 0) {
        return [];
    }

    return Array.from({ length: +h }, () =>
        Array.from({ length: +w }, () => " ")
    );
};
