import { Canvas } from "../canvas";
import { RectangleCommand } from "../commands";
import { drawLine } from "./draw-line";

export const drawRectangle = (
    canvas: Canvas,
    [, x1, y1, x2, y2]: RectangleCommand
): Canvas => {
    if (!canvas?.[+y1 - 1]?.[+x1 - 1] || !canvas?.[+y2 - 1]?.[+x2 - 1]) {
        return canvas;
    }

    // top left to topright
    drawLine(canvas, ["L", x1, y1, x2, y1]);
    // top right to bottom right
    drawLine(canvas, ["L", x2, y1, x2, y2]);
    // bottom right to bottom left
    drawLine(canvas, ["L", x1, y2, x2, y2]);
    // bottom left to top left
    drawLine(canvas, ["L", x1, y1, x1, y2]);

    return canvas;
};
