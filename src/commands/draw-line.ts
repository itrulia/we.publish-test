import { Canvas } from "../canvas";
import { LineCommand } from "../commands";

const drawHorizontalLine = (
    canvas: Canvas,
    x1: number,
    x2: number,
    y: number
): Canvas => {
    for (let index = x1 - 1; index < x2; index++) {
        canvas[y - 1][index] = "x";
    }

    return canvas;
};

const drawVerticalLine = (
    canvas: Canvas,
    y1: number,
    y2: number,
    x: number
): Canvas => {
    for (let index = y1 - 1; index < y2; index++) {
        canvas[index][x - 1] = "x";
    }

    return canvas;
};

export const drawLine = (
    canvas: Canvas,
    [, x1, y1, x2, y2]: LineCommand
): Canvas => {
    if (!canvas?.[+y1 - 1]?.[+x1 - 1] || !canvas?.[+y2 - 1]?.[+x2 - 1]) {
        return canvas;
    }

    if (x1 === x2) {
        return drawVerticalLine(canvas, +y1, +y2, +x1);
    }

    if (y1 === y2) {
        return drawHorizontalLine(canvas, +x1, +x2, +y1);
    }

    return canvas;
};
