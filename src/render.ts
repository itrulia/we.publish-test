import { Canvas } from "./canvas";

export const renderCanvas = (canvas: Canvas | null): string => {
    if (!canvas || !canvas.length || !canvas[0].length) {
        return "";
    }

    let output = `\n`;

    /**
     * xCoord/yCoord starts at -1 and ends at Array.lenght+1 so we can draw the border
     */
    for (let yCoord = -1; yCoord < canvas.length + 1; yCoord++) {
        for (let xCoord = -1; xCoord < canvas[0].length + 1; xCoord++) {
            if (!canvas[yCoord]) {
                output = `${output}-`;
                continue;
            }

            if (!canvas[yCoord][xCoord]) {
                output = `${output}|`;
                continue;
            }

            output += canvas[yCoord][xCoord];
        }

        output = `${output}\n`;
    }

    return output;
};
