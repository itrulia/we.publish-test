import { executeCommand } from "./commands/execute";
import { renderCanvas } from "./render";

test("it should render a new canvas", () => {
    let canvas = [] as string[][];

    canvas = executeCommand(canvas, ["C", "20", "4"]);
    const result = renderCanvas(canvas);

    expect(result).toEqual(
        `
----------------------
|                    |
|                    |
|                    |
|                    |
----------------------
`
    );
});

test("it should render a horizontal line", () => {
    let canvas = [] as string[][];

    canvas = executeCommand(canvas, ["C", "20", "4"]);
    canvas = executeCommand(canvas, ["L", "1", "2", "6", "2"]);
    const result = renderCanvas(canvas);

    expect(result).toEqual(
        `
----------------------
|                    |
|xxxxxx              |
|                    |
|                    |
----------------------
`
    );
});

test("it should render a vertical line", () => {
    let canvas = [] as string[][];

    canvas = executeCommand(canvas, ["C", "20", "4"]);
    canvas = executeCommand(canvas, ["L", "1", "2", "6", "2"]);
    canvas = executeCommand(canvas, ["L", "6", "3", "6", "4"]);
    const result = renderCanvas(canvas);

    expect(result).toEqual(
        `
----------------------
|                    |
|xxxxxx              |
|     x              |
|     x              |
----------------------
`
    );
});

test("it should render a rectangle", () => {
    let canvas = [] as string[][];

    canvas = executeCommand(canvas, ["C", "20", "4"]);
    canvas = executeCommand(canvas, ["L", "1", "2", "6", "2"]);
    canvas = executeCommand(canvas, ["L", "6", "3", "6", "4"]);
    canvas = executeCommand(canvas, ["R", "16", "1", "20", "3"]);
    const result = renderCanvas(canvas);

    expect(result).toEqual(
        `
----------------------
|               xxxxx|
|xxxxxx         x   x|
|     x         xxxxx|
|     x              |
----------------------
`
    );
});

test("it should render a bucket fill", () => {
    let canvas = [] as string[][];

    canvas = executeCommand(canvas, ["C", "20", "4"]);
    canvas = executeCommand(canvas, ["L", "1", "2", "6", "2"]);
    canvas = executeCommand(canvas, ["L", "6", "3", "6", "4"]);
    canvas = executeCommand(canvas, ["R", "16", "1", "20", "3"]);
    canvas = executeCommand(canvas, ["B", "10", "3", "o"]);
    const result = renderCanvas(canvas);

    expect(result).toEqual(
        `
----------------------
|oooooooooooooooxxxxx|
|xxxxxxooooooooox   x|
|     xoooooooooxxxxx|
|     xoooooooooooooo|
----------------------
`
    );
});
