import {
    isBucketFillCommand,
    isCanvasCommand,
    isLineCommand,
    isQuitCommand,
    isRectangleCommand,
} from "./commands";

test("should detect if it's a canvas command", () => {
    const result1 = isCanvasCommand(["C", "20", "4"]);
    expect(result1).toBeTruthy();

    const result2 = isCanvasCommand(["c", "20", "4"]);
    expect(result2).toBeTruthy();

    const result3 = isCanvasCommand(["Q"]);
    expect(result3).toBeFalsy();

    const result4 = isCanvasCommand([] as any);
    expect(result4).toBeFalsy();
});

test("should detect if it's a line command", () => {
    const result1 = isLineCommand(["L", "20", "4", "20", "4"]);
    expect(result1).toBeTruthy();

    const result2 = isLineCommand(["l", "20", "4", "20", "4"]);
    expect(result2).toBeTruthy();

    const result3 = isLineCommand(["Q"]);
    expect(result3).toBeFalsy();

    const result4 = isLineCommand([] as any);
    expect(result4).toBeFalsy();
});

test("should detect if it's a rectangle command", () => {
    const result1 = isRectangleCommand(["R", "20", "4", "20", "4"]);
    expect(result1).toBeTruthy();

    const result2 = isRectangleCommand(["R", "20", "4", "20", "4"]);
    expect(result2).toBeTruthy();

    const result3 = isRectangleCommand(["L", "20", "4", "20", "4"]);
    expect(result3).toBeFalsy();

    const result4 = isRectangleCommand([] as any);
    expect(result4).toBeFalsy();
});

test("should detect if it's a bucket fill command", () => {
    const result1 = isBucketFillCommand(["B", "20", "4", "c"]);
    expect(result1).toBeTruthy();

    const result2 = isBucketFillCommand(["b", "20", "4", "c"]);
    expect(result2).toBeTruthy();

    const result3 = isBucketFillCommand(["Q"]);
    expect(result3).toBeFalsy();

    const result4 = isBucketFillCommand([] as any);
    expect(result4).toBeFalsy();
});

test("should detect if it's a quit command", () => {
    const result1 = isQuitCommand(["Q"]);
    expect(result1).toBeTruthy();

    const result2 = isQuitCommand(["q"]);
    expect(result2).toBeTruthy();

    const result3 = isQuitCommand(["B", "20", "4", "c"]);
    expect(result3).toBeFalsy();

    const result4 = isQuitCommand([] as any);
    expect(result4).toBeFalsy();
});
